
# Debian Quick Image Baker (`dqib`)

This is a quick-and-dirty (dirtier than quick, as a matter of facts)
script to create a Debian image for a given architecture that can be
run with QEMU. It can be useful for playing with exotic architectures
that you might not have available in real hardware or for porting
Debian packages when you cannot for some reason use the official
porterboxes (see below for more reasons).

The script creating these images is just a thin layer over
qemu-debootstrap, plus some suggestion on the QEMU command line to
use. The script is nowhere near debian-installer in terms of
completeness, so created images are probably not suitable for
production use. Just use them for playing and development.

Pre-baked images are automatically generated weekly and [available for
download](https://people.debian.org/~gio/dqib/).

## How to use it

Basically you have to run as root the script `create.sh` passing as
only argument the type of system for which you want to create an
image. The script should not touch anything outside a temporary
directory created inside `/tmp`, but if (rightfully so) do not like to
run random scripts as root, then a `Dockerfile` is provided that
creates a docker container with the packages you need already
installed (the script still has to be run as root, but at least there
is a container around it). Unfortunately the root requirement comes
from `debootstrap` and cannot be removed.

You can also pass some environment variables to `create.sh` to change
some default settings. Read the script (it is very simple!) to know
which environment variables and system types are supported.

For example:

    $ docker build . -t dqib
    $ docker run -it dqib
    # cd
    # DISK_SIZE=15G MEM=512M ./create.sh mipsel-malta

At the end, the creation script will print a QEMU call line that
should just work and that you can customize according to your wishes
(in particolar, for machines that support graphics hardware you might
want to remove `-nographic` and `console` commands).

All machines have root password set to `root` and an unprivileged user
named `debian` with password `debian`. The hostname is also
`debian`. The machine should automatically have Internet connection if
the host computer has, and an SSH server should be running. QEMU will
automatically forward connections to the host port 2222 to the SSH
server inside.

You can extract the generated files from the Docker container with
`docker cp`. You need the disk image (`image.qcow2`), the kernel
(`kernel`) and the initrd (`initrd`). Since QEMU is directly fed the
kernel and the initrd, when you install a new kernel or regenerate the
initrd you need to copy them out of the filesystem for QEMU to pick
them up.

Have fun!

## Feedback

I am not an expert of most if not all architectures for which `dqib`
tries to generate images. Thinking about it, I am probably not an
expert of any architecture at all. I have just tried different QEMU
flags until I managed to get it more or less working. If you happen to
know better then me or have useful suggestions anyway, please [open
issues on the `dqib` project](https://gitlab.com/giomasce/dqib/issues)
write me at `gio@debian.org`.

## To do

 * Add more architectures (also from Debian ports); in line of
   principle it seems that all architectures in Debian and Debian
   ports are supported by QEMU, except `ia64`, although some of them
   are probably not really in shape to run a full Linux system.

 * Add support for non-Linux ports. I have no idea of how this can be
   done. I believe the trick of `qemu-bootstrap` cannot be done with
   non-Linux systems.

## Why this thing?

 * Because sometimes using QEMU is more convenient than accessing
   Debian porterboxes (i.e., you have no or bad Internet connection,
   you need root access, you need to mess up with the filesystem or
   the network, you do not have an account on Debian boxes, there are
   no porterboxes for some architecture, whatever reason you want).

 * Because there are Debian images for QEMU around, but sometimes
   there are outdated and there is no script around to regenerated
   them (and also customize them).

 * Because Debian supports a lot of architectures and QEMU supports a
   lot of architectures, and it is a shame not to have a quick way to
   pair up these two lots!

 * Because I wanted to have some fun with architectures I do not
   usually play with, and I hope this will be useful for other people.

## License

This work is covered by the MIT license.

    Copyright © 2019 Giovanni Mascellani <gio@debian.org>

    Permission is hereby granted, free of charge, to any person obtaining
    a copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:

    The above copyright notice and this permission notice shall be
    included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
    OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
    WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
